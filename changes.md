**Code Review**

*  Added a message to display the total cost, before prompting the user for cash tendered. This was done because customers had no idea what to pay 9a632c10865a4711cc9871e6d7c5e8d9f8f55d8b

* Major rewrite due to confusing variables and hacky / buggy code f5772daaeefe1199a2491ed5d5386ea38cf71c52

* Fixed the Receipt class; to implement the new things created in milestone 3 and the change amount a40c9bf5b6819a8f573c887645c74624dc83bb47 , a99185df7513054d486ee660e5ea88d9ab7a3118

* Updated the discountPrice function to call the right function 5b6b70a1fd54488354bf176c066c8286f5206b80

*  Added a card number validation to make sure the card number is 9 digits in length. e4ada1a70395e8ad2b90af311cec5409935a7631