import java.util.ArrayList;
import java.util.Scanner;

public class CashRegister {
	private static boolean execute = true;
	private static double balance, change, tendered, totalCost = 0;
	private static ArrayList<Transaction> transactionsList = new ArrayList<>();
	private static Scanner in = new Scanner(System.in);
	private static Receipt rec;
	
	public static void main(String[] args) {
		System.out.println("Hello. Welcome to cash register.");
		balance = numberPrompt("Please enter the Cash Register's balance: $");
		responsePrompt("\n*** Start transaction? (Y/N) ***: ", false, true);
		
		if (execute) {
			do {
				transactionsList.add(new Transaction(stringPrompt("\nPlease enter the item's name: "), 
						numberPrompt("Please enter the item's cost: $")));
				responsePrompt("\n*** Next item? (Y/N) ***: ", false, true);
			} while (execute);
			
			for (Transaction trans : transactionsList) {
				totalCost += trans.getCost();
			}
			
			System.out.println("TOTAL: $" + totalCost);
			tendered = numberPrompt("\nPlease enter the cash amount tendered: $");
			rec = new Receipt(transactionsList,totalCost,tendered,change);
			
			if (tendered >= totalCost) {
				balance += totalCost;
				change = tendered - totalCost;
				System.out.println("\nAmount of change is $" + change + ".");
				responsePrompt("\n*** Would you like a receipt? (Y/N) ***: ", true, false);
				
			} else {
				System.out.println("\n*** Insufficient fund! Minimum $" + totalCost + " required. ***");
			}
		}
		
		in.close();
		System.out.println("\nThank you for using our service!");
		System.out.println("Current balance of this cash register: $" + balance);
	}
	

	
	private static double numberPrompt (String prompt) {
		boolean valid;
		double number = 0;
		do {
			valid = true;
			System.out.print(prompt);
			try {
				number = Double.parseDouble(in.nextLine());
			} catch (NumberFormatException e) {
				valid = false;
				System.out.println("\n*** Invalid input! ***\n");
			}
		} while (!valid);
		return number;
	}
	
	private static void responsePrompt(String prompt, boolean injectReceipt, boolean noExecution) {
		boolean valid;
		String response;
		do {
			valid = true;
			System.out.print(prompt);
			response = in.nextLine();
			switch (response.toLowerCase()) {
				case "y":
					if (injectReceipt) rec.printReceipt();
					break;
				case "n":
					if (noExecution) execute = false;
					break;
				default:
					valid = false;
					System.out.println("\n*** Invalid input! ***");
					break;
			}
		} while (!valid);
	}
	
	private static String stringPrompt (String prompt) {
		System.out.print(prompt);
		return in.nextLine();
	}
}
