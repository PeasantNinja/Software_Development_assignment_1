import java.util.ArrayList;
import java.util.Random;


public class Receipt {
	private ArrayList<Transaction> transList;
	private String servedBy;
	
	private double total,tender,changeGiven;
	
	private int receiptID;
	
	public Receipt(ArrayList<Transaction> trans,double totalCost, double tendered,double change) {
		this.transList = trans;
		this.total = totalCost;
		this.tender = tendered;
		this.changeGiven = change;
		Random ran = new Random();
	    this.receiptID = ran.nextInt(999999);
	}
	
	public void printReceipt() {
		System.out.println("\n|*** *** Receipt *** **|");
		System.out.println("|  ====  Items  ====   |");
		
		for (Transaction trans : transList) {
			System.out.println("| " + trans.getName() + " - $" + trans.getCost());
		}
		
		System.out.println("=====");
		System.out.println("| Total:    $" + total);
		System.out.println("| Tendered: $" + tender);
		System.out.println("| Change  : $" + changeGiven);
		System.out.println("=====");
		System.out.println("| Receipt ID: " + this.receiptID);
		System.out.println("| ");
		System.out.println(generateBarcode());

		
	}
	
	public String generateBarcode() {
		String barcode =  "";
		Random ran = new Random();
		int count =0;
		
		while(count < 8) {
			int newInt = ran.nextInt(2)%2;
			barcode += newInt;
			count++;
		}

		String displayBarcode = "|     * ";
		
		for(char ch: barcode.toCharArray()) {
			if(ch == '1') {
				displayBarcode += "|";
			}
			else {
				displayBarcode +=  " ";
			}
		}
		displayBarcode += " *";
		
		displayBarcode += "\n" + displayBarcode;
		displayBarcode += "\n|     * " + barcode  + " *";
		displayBarcode =  "|     +==========+\n" + displayBarcode + "\n|     +==========+";
		
		
		
		
		return displayBarcode;
		
	}
	

}
